'use strict';

const _      = require('lodash');
const path   = require('path');
const fs     = require('fs');
const yaml   = require('js-yaml');
const async  = require('neo-async');
const defend = require('xana-defend');

/**
 * Common config key.
 * @type {String}
 * @const
 * @private
 */
const DEFAULT_CONFIG_KEY = 'default';

/**
 * A map of variables.
 * @type {Object}
 * @private
 */
let __var = {};

/**
 * A map of configs.
 * @type {Object}
 * @private
 */
let __conf = {};

/**
 * Custom YAML type for variable.
 * @const
 */
const VAR_YAML_TYPE = new yaml.Type('!var', {
	kind: 'scalar',
	construct(varName) {
		const varValue = __var[varName];
		if (varValue === undefined) {
			throw new Error(`Not found variable "${varName}"`);
		}
		return varValue;
	}
});

/**
 * Custom YAML schema.
 * @const
 */
const CUSTOM_YAML_SCHEMA = yaml.Schema.create([
	VAR_YAML_TYPE
]);

/**
 * Default options for load().
 * @const
 */
const DEFAULT_LOAD_OPTS = {
	mock: {
		readDir: fs.readdir,
		readFile: fs.readFile,
		env: process.env.NODE_ENV,
	},
	yaml: {
		schema: CUSTOM_YAML_SCHEMA
	}
};

/**
 * List all config files in a config dir.
 * @param {String} dir
 * @param {Object} opts
 * @param {Function} done
 * @private
 */
function listConfigDir(dir, opts, done) {
	async.waterfall([
		(next) => opts.mock.readDir(dir, next),
		(files, next) => next(null, files.filter(file => file.endsWith('.yaml'))),
		(files, next) => next(null, files.map(file => path.join(dir, file)))
	], done);
}

/**
 * Load config data.
 * @param {String} data
 * @param {Object} opts
 * @returns {*} The config.
 * @private
 */
function loadConfigData(data, opts) {
	const config  = yaml.load(data, opts.yaml);
	const defConf = config[DEFAULT_CONFIG_KEY];
	const envConf = config[opts.mock.env];
	return _.merge({}, defConf, envConf);
}

/**
 * Load a config file.
 * @param {String} file
 * @param {Object} opts
 * @param {Function} done
 * @private
 */
function loadConfigFile(file, opts, done) {
	const confName = path.basename(file, '.yaml');
	async.waterfall([
		(next) => opts.mock.readFile(file, next),
		(data, next) => next(null, {[confName]: loadConfigData(data, opts)})
	], done);
}

/**
 * Load a config dir.
 * @param {String} confDir
 * @param {Object} opts
 * @param {Function} done
 * @private
 */
function loadConfigDir(confDir, opts, done) {
	const _loadConfigFile = _.partialRight(loadConfigFile, opts, _);
	async.waterfall([
		(next) => listConfigDir(confDir, opts, next),
		(files, next) => async.map(files, _loadConfigFile, next),
		(configs, next) => next(null, configs.reduce(_.merge, {}))
	], done);
}

/**
 * Provide a map of variables.
 * @param {Object} varMap
 */
function useVar(varMap) {
	if (!_.isPlainObject(varMap)) {
		throw new TypeError(`useVar expects a plain object but got "${varMap}"`);
	}
	__var = varMap;
}

/**
 * Load configs from top-level files of a directory.
 * @param {String} confDir
 * @param {Object} [opts]
 * @param {Function} done
 */
function load(confDir, opts, done) {
	if (_.isFunction(opts)) {
		done = opts;
		opts = {};
	}
	opts = _.defaults({}, opts, DEFAULT_LOAD_OPTS);
	return loadConfigDir(confDir, opts, (err, conf) => {
		__conf = conf;
		done(err, get());
	});
}

/**
 * Get a config by its path.
 * @param {String} [path]
 * @param {Object} [opts]
 * @param {Boolean} [opts.strict]
 * @throws {ReferenceError}
 */
function get(path, opts) {
	opts = _.defaults({}, opts, {
		strict: true
	});

	const config = path ? _.get(__conf, path) : __conf;
	if (config === undefined) {
		throw new ReferenceError(
			`Config at "${path}" does not exist.`
		);
	}

	if (opts.strict && _.isPlainObject(config)) {
		return defend(config);
	} else {
		return config;
	}
}

module.exports = {
	useVar,
	load,
	get
};
