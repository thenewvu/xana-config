'use strict';

const fs      = require('fs');
const path    = require('path');
const yaml    = require('js-yaml');
const decache = require('decache');
const expect  = require('chai').expect;

const testDir       = path.dirname(__filename);
const testDataFiles = fs.readdirSync(testDir)
												.filter(file => file.endsWith('.yaml'))
												.map(file => path.join(testDir, file));

testDataFiles.forEach(file => {
	const testCases = yaml.load(fs.readFileSync(file));
	Object.keys(testCases).forEach((describeName) => {
		describe(describeName, function () {
			Object.keys(testCases[describeName]).forEach((itName) => {
				it(itName, function (done) {
					const itData = testCases[describeName][itName];
					decache('../src/index.js');
					const config   = require('../src/index.js');
					const loadOpts = {
						mock: {
							readDir(dir, done) {
								done(null, Object.keys(itData.req));
							},
							readFile(file, done) {
								done(null, itData.req[file]);
							},
							env: itData.env
						}
					};
					config.useVar(itData.var || {});
					config.load('', loadOpts, (err) => {
						expect(err).to.deep.equal(itData.err);
						expect(config.get()).to.deep.equal(itData.res);
						done();
					});
				});
			});
		});
	});
});
